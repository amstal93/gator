#!/bin/bash
set -x

sudo docker build -t akozak1lab/gator:v3.8.0 .
sudo docker tag akozak1lab/gator:3.8.0 akozak1lab/gator:latest

if [[ $* == *--push* ]]; then
    sudo docker push akozak1lab/gator:3.8.0
    sudo docker push akozak1lab/gator:latest
fi
